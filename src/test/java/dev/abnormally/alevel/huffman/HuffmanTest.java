package dev.abnormally.alevel.huffman;

import dev.abnormally.alevel.huffman.active.Compressor;
import dev.abnormally.alevel.huffman.active.Decompressor;
import dev.abnormally.alevel.huffman.active.independent.Press;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

public class HuffmanTest {
    final Logger logger = Logger.getLogger("Huffman");

    private File inputFile;
    private File compressedFile;
    private File decompressedFile;

    @Before
    public void prepareFiles() throws IOException {
        inputFile = new File("test.txt");
        compressedFile = new File("test.txt.hf");
        decompressedFile = new File("test.decompressed.txt");

        Press compressor = new Compressor();
        compressor.call(inputFile.getAbsolutePath());

        Press decompressor = new Decompressor();
        decompressor.setCompress(false);
        decompressor.call(compressedFile.getAbsolutePath(), decompressedFile.getAbsolutePath());
    }

    @Test
    public void decompressedTest() {
        if (!decompressedFile.exists() || !decompressedFile.isFile() || !decompressedFile.canRead()) {
            throw new AssertionError("Восстановленного файла не существует.");
        }
    }

    @Test
    public void filesMatch() throws IOException {
        InputStream original = new FileInputStream(inputFile);
        InputStream decompressed = new FileInputStream(decompressedFile);

        Assert.assertEquals("Размеры доступных данных не совпадают", original.available(), decompressed.available());

        int counter = 0;
        while (original.available() != 0 && decompressed.available() != 0) {
            int originalByte = original.read();
            int decompressedByte = decompressed.read();

            Assert.assertEquals(String.format("Байты в позиции %d не совпадает.", counter), originalByte, decompressedByte);

            counter++;
        }

        original.close();
        decompressed.close();
    }

    @Test
    public void sizes() {
        if (compressedFile.length() > inputFile.length()) {
            // Actually always false for current test file (test.txt)
            logger.warning("Сжатый файл больше оригинального. Обычно такое происходит когда пытаются пережать уже сжатый файл.");
        }
    }

}
