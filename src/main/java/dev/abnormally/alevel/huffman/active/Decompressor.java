package dev.abnormally.alevel.huffman.active;

import dev.abnormally.alevel.huffman.active.independent.Press;
import dev.abnormally.alevel.huffman.data.Table;
import dev.abnormally.alevel.huffman.io.Decoder;
import dev.abnormally.alevel.huffman.io.stream.BitIn;

import java.io.*;

public class Decompressor extends Press {

    public void action(File inputFile, File outputFile) throws IOException {
        try (BitIn in = new BitIn(new BufferedInputStream(new FileInputStream(inputFile)))) {
            try (OutputStream out = new BufferedOutputStream(new FileOutputStream(outputFile))) {
                decompress(in, out);
            }
        }
    }

    void decompress(BitIn in, OutputStream out) throws IOException {
        if (isCompress) throw new IllegalArgumentException("Файл не соответствует ожиданиям.");

        frequencies = new Table(fqs);
        coder = new Decoder(in);
        coder.setTree(frequencies.makeCodeTree());

        while (true) {
            currentChunk = ((Decoder) coder).read();

            if (currentChunk == 256 || currentChunk == -1 || currentChunk == (int) '\0')
                break;

            out.write(currentChunk);
            frequencyIterator();
        }
    }

}
