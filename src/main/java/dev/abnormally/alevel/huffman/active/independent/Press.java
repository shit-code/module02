package dev.abnormally.alevel.huffman.active.independent;

import dev.abnormally.alevel.huffman.data.Table;
import dev.abnormally.alevel.huffman.io.contract.Coder;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public abstract class Press {
    protected boolean isCompress = true;

    protected int[] fqs = new int[257];

    {
        Arrays.fill(fqs, 1);
    }

    protected int currentChunk;

    protected Table frequencies;
    protected Coder coder;
    private int counter = 0;

    public void call(String inputFileName) throws IOException {
        final StringBuilder outputFileName = new StringBuilder();

        String[] inputSplit = inputFileName.trim().split("\\.");

        if (inputSplit[inputSplit.length - 1].equals("hf")) {
            isCompress = false;
            Arrays.stream(inputSplit).forEach(part -> outputFileName.append(part.equals("hf") ? "" : part).append('.'));
            outputFileName.setLength(outputFileName.length() - 2);
        } else {
            outputFileName.append(inputFileName).append(".hf");
        }

        call(inputFileName, outputFileName.toString());
    }

    public void call(String inputFileName, String outputFileName) throws IOException {
        action(new File(inputFileName), new File(outputFileName));
    }

    abstract public void action(File inputFile, File outputFile) throws IOException;

    private static boolean isPowerOf2(int x) {
        return x > 0 && Integer.bitCount(x) == 1;
    }

    protected void frequencyIterator() {
        counter++;

        frequencies.increment(currentChunk);

        if (counter < 262144 && isPowerOf2(counter) || counter % 262144 == 0)
            coder.setTree(frequencies.makeCodeTree());

        if (counter % 262144 == 0) frequencies = new Table(fqs);
    }

    public boolean isCompress() {
        return isCompress;
    }

    public void setCompress(boolean compress) {
        isCompress = compress;
    }

}