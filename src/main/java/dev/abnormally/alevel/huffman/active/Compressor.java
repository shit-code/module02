package dev.abnormally.alevel.huffman.active;

import dev.abnormally.alevel.huffman.active.independent.Press;
import dev.abnormally.alevel.huffman.data.Table;
import dev.abnormally.alevel.huffman.io.Encoder;
import dev.abnormally.alevel.huffman.io.stream.BitOut;

import java.io.*;

public class Compressor extends Press {

    public void action(File inputFile, File outputFile) throws IOException {
        try (InputStream in = new BufferedInputStream(new FileInputStream(inputFile))) {
            try (BitOut out = new BitOut(new BufferedOutputStream(new FileOutputStream(outputFile)))) {
                compress(in, out);
            }
        }
    }

    void compress(InputStream in, BitOut out) throws IOException {
        if (!isCompress) throw new IllegalArgumentException("Файл не соответствует ожиданиям.");

        frequencies = new Table(fqs);
        coder = new Encoder(out);

        coder.setTree(frequencies.makeCodeTree());

        while (true) {
            currentChunk = in.read();

            if (currentChunk == -1)
                break;

            ((Encoder) coder).write(currentChunk);
            frequencyIterator();
        }

        ((Encoder) coder).write('\0');
    }

}
