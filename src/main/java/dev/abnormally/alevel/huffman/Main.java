package dev.abnormally.alevel.huffman;

import dev.abnormally.alevel.huffman.active.Compressor;
import dev.abnormally.alevel.huffman.active.Decompressor;
import dev.abnormally.alevel.huffman.active.independent.Press;
import picocli.CommandLine;

import java.io.IOException;

@CommandLine.Command(name = "Huffman", mixinStandardHelpOptions = true, version = "0.1")
public class Main implements Runnable {

    public static void main(String[] args) {
        Main man = new Main();

        CommandLine.run(man, args);
    }

    @CommandLine.Option(names = {"-d", "--decompress"}, description = "Явное указание, что файл нужно разжать.")
    private boolean isDecompress = false;

    @CommandLine.Option(names = {"-c", "--compress"}, description = "Явное указание, что файл нужно сжать.")
    private boolean isCompress = false;

    @CommandLine.Option(names = {"-o", "--output"}, paramLabel = "<file-name>", description = "Явное указание названия выходного файла.")
    private String output;

    @CommandLine.Parameters(arity = "0", paramLabel = "<file-name>", description = "Файл, над которым нужно произвести действие")
    private String input;

    public void run() {
        if (isCompress && isDecompress) throw new AssertionError("Только одно действие за раз.");

        Press press = null;
        boolean pressing = !input.contains(".hf");

        if (isCompress) {
            press = new Compressor();
            press.setCompress(true);
        }

        if (isDecompress) {
            press = new Decompressor();
            press.setCompress(false);
        }

        if (!isCompress && !isDecompress) {
            if (pressing) {
                press = new Compressor();
                press.setCompress(true);
            } else {
                press = new Decompressor();
                press.setCompress(false);
            }
        }

        try {
            if (output == null) press.call(input);
            else press.call(input, output);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e.getCause());
        }

        System.out.println("Done");
    }

}
