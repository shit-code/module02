package dev.abnormally.alevel.huffman.io;

import dev.abnormally.alevel.huffman.data.Tree;
import dev.abnormally.alevel.huffman.io.contract.Coder;
import dev.abnormally.alevel.huffman.io.stream.BitOut;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class Encoder implements Coder {
    private final BitOut output;
    private Tree tree;

    public Encoder(BitOut out) {
        output = Objects.requireNonNull(out);
    }

    public void write(int symbol) throws IOException {
        if (getTree() == null) throw new NullPointerException("Древонул");

        List<Integer> bits = getTree().getCode(symbol);

        for (int b : bits) output.write(b);
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

}
