package dev.abnormally.alevel.huffman.io.stream;

import dev.abnormally.alevel.huffman.io.stream.contract.BitStream;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

public class BitOut implements AutoCloseable, BitStream {
    private OutputStream out;
    private int current;
    private int filled;

    public BitOut(OutputStream out) {
        this.out = Objects.requireNonNull(out);
        current = 0;
        filled = 0;
    }

    public void write(int b) throws IOException {
        if (b != 0 && b != 1) throw new IllegalArgumentException("Это бит, ало");

        current = (current << 1) | b;
        filled++;

        if (filled == 8) {
            out.write(current);
            current = 0;
            filled = 0;
        }
    }

    public void close() throws IOException {
        while (filled != 0)
            write(0);
        out.close();
    }

}
