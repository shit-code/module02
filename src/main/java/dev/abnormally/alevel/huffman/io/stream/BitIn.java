package dev.abnormally.alevel.huffman.io.stream;

import dev.abnormally.alevel.huffman.io.stream.contract.BitStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class BitIn implements AutoCloseable, BitStream {
    private InputStream in;
    private int current;
    private int remaining;

    public BitIn(InputStream in) {
        this.in = Objects.requireNonNull(in);
        current = 0;
        remaining = 0;
    }

    public int read() throws IOException {
        if (current == -1)
            return -1;

        if (remaining == 0) {
            current = in.read();
            if (current == -1)
                return -1;
            remaining = 8;
        }

        if (remaining <= 0)
            throw new AssertionError();
        remaining--;

        return (current >>> remaining) & 1;
    }

    public void close() throws IOException {
        in.close();
        current = -1;
        remaining = 0;
    }

}
