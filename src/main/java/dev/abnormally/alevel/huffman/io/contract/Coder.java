package dev.abnormally.alevel.huffman.io.contract;

import dev.abnormally.alevel.huffman.data.Tree;

public interface Coder {

    Tree getTree();

    void setTree(Tree tree);

}
