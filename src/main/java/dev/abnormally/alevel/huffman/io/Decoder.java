package dev.abnormally.alevel.huffman.io;

import dev.abnormally.alevel.huffman.data.Tree;
import dev.abnormally.alevel.huffman.data.node.Hold;
import dev.abnormally.alevel.huffman.data.node.Leaf;
import dev.abnormally.alevel.huffman.data.node.contract.Node;
import dev.abnormally.alevel.huffman.io.contract.Coder;
import dev.abnormally.alevel.huffman.io.stream.BitIn;

import java.io.IOException;
import java.util.Objects;

public class Decoder implements Coder {
    private final BitIn input;
    private Tree tree;

    public Decoder(BitIn in) {
        input = Objects.requireNonNull(in);
    }

    public int read() throws IOException {
        if (tree == null) throw new NullPointerException("Древонул");

        Node currentNode = tree.getRoot();

        int temp;
        while ((temp = input.read()) > -1) {
            Node nextNode;

            if (temp == 0) nextNode = ((Hold) currentNode).getLeft();
            else if (temp == 1) nextNode = ((Hold) currentNode).getRight();
            else throw new AssertionError("Что-то пошло не так");

            switch (nextNode.getType()) {
                case hold:
                    currentNode = nextNode;

                    break;
                case leaf:
                    return ((Leaf) nextNode).getChunk();
                default:
                    System.out.println("Неизвестный тип ноды. Проехали.");
            }
        }

        return -1;
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

}
