package dev.abnormally.alevel.huffman.data;

import dev.abnormally.alevel.huffman.data.node.Hold;
import dev.abnormally.alevel.huffman.data.node.Leaf;
import dev.abnormally.alevel.huffman.data.node.contract.Node;

import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;

public class Table {
    private int[] frequencies;

    public Table(int[] fqs) {
        Objects.requireNonNull(fqs);

        if (fqs.length < 2) throw new IllegalArgumentException("Минимум два");

        frequencies = fqs.clone();

        for (int x : frequencies) {
            if (x < 0) throw new IllegalArgumentException("Отрицательная частота");
        }
    }

    public int get(int chunk) {
        checkChunk(chunk);

        return frequencies[chunk];
    }

    public void set(int chunk, int freq) {
        checkChunk(chunk);

        if (freq < 0) throw new IllegalArgumentException("Отрицательная частота");

        frequencies[chunk] = freq;
    }

    public void increment(int chunk) {
        checkChunk(chunk);

        if (frequencies[chunk] == Integer.MAX_VALUE) throw new IllegalStateException("Максимальная частота не может превышать Integer.MAX_VALUE, простите.");

        frequencies[chunk]++;
    }

    private void checkChunk(int chunk) {
        if (chunk < 0 || chunk >= frequencies.length) throw new IllegalArgumentException("Чанк вне зоны доступа");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < frequencies.length; i++) sb.append(String.format("%d\t%d%n", i, frequencies[i]));

        return sb.toString();
    }

    public Tree makeCodeTree() {
        Queue<NodeWithFrequency> frequencyQueue = new PriorityQueue<>();

        for (int i = 0; i < frequencies.length; i++) {
            if (frequencies[i] > 0) frequencyQueue.add(new NodeWithFrequency(new Leaf(i), i, frequencies[i]));
        }

        for (int i = 0; i < frequencies.length && frequencyQueue.size() < 2; i++) {
            if (frequencies[i] == 0) frequencyQueue.add(new NodeWithFrequency(new Leaf(i), i, 0));
        }

        if (frequencyQueue.size() < 2) throw new AssertionError();

        while (frequencyQueue.size() > 1) {
            NodeWithFrequency left = frequencyQueue.remove();
            NodeWithFrequency right = frequencyQueue.remove();

            frequencyQueue.add(
                    new NodeWithFrequency(
                            new Hold(left.node, right.node),
                            Math.min(left.lowestChunk, right.lowestChunk),
                            left.frequency + right.frequency
                    )
            );
        }

        return new Tree(frequencyQueue.remove().node, frequencies.length);
    }

    private static class NodeWithFrequency implements Comparable<NodeWithFrequency> {
        final Node node;
        final int lowestChunk;
        final long frequency;

        public NodeWithFrequency(Node someNode, int lowChunk, long freq) {
            node = someNode;
            frequency = freq;
            lowestChunk = lowChunk;
        }

        public int compareTo(NodeWithFrequency other) {
            if (frequency < other.frequency)
                return -1;
            else if (frequency > other.frequency)
                return 1;
            else return Integer.compare(lowestChunk, other.lowestChunk);
        }

    }

}
