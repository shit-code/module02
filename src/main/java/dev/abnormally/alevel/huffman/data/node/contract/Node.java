package dev.abnormally.alevel.huffman.data.node.contract;

public interface Node {

    NodeType getType();

}
