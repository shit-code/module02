package dev.abnormally.alevel.huffman.data.node;

import dev.abnormally.alevel.huffman.data.node.contract.Node;
import dev.abnormally.alevel.huffman.data.node.contract.NodeType;

import java.util.Objects;

public class Hold implements Node {
    private static final String emptyNodeError = "Узел не может быть пустым";
    private static final NodeType type = NodeType.hold;
    private final Node right;
    private final Node left;

    public Hold(Node left, Node right) {
        this.right = Objects.requireNonNull(left, emptyNodeError);
        this.left = Objects.requireNonNull(right, emptyNodeError);
    }

    public Node getRight() {
        return right;
    }

    public Node getLeft() {
        return left;
    }

    @Override
    public NodeType getType() {
        return type;
    }

}
