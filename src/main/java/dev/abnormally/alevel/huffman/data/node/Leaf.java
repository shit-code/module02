package dev.abnormally.alevel.huffman.data.node;

import dev.abnormally.alevel.huffman.data.node.contract.Node;
import dev.abnormally.alevel.huffman.data.node.contract.NodeType;

public class Leaf implements Node {
    private static final NodeType type = NodeType.leaf;
    private final int chunk;

    public Leaf(int dataChunk) {
        if (dataChunk < 0) throw new IllegalArgumentException("Дата не может быть отрицательной");

        chunk = dataChunk;
    }

    public int getChunk() {
        return chunk;
    }

    @Override
    public NodeType getType() {
        return type;
    }

}
