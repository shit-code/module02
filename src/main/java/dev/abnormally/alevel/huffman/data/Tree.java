package dev.abnormally.alevel.huffman.data;

import dev.abnormally.alevel.huffman.data.node.Hold;
import dev.abnormally.alevel.huffman.data.node.Leaf;
import dev.abnormally.alevel.huffman.data.node.contract.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Tree {
    private final Node root;
    private List<List<Integer>> codes;

    public Tree(Node root, int symbolLimit) {
        this.root = Objects.requireNonNull(root);

        if (symbolLimit < 2) throw new IllegalArgumentException("Дайте минимум пару чанков");

        codes = new ArrayList<>();

        for (int i = 0; i < symbolLimit; i++)
            codes.add(null);

        buildCodeList(root, new ArrayList<>());
    }

    private void buildCodeList(Node node, List<Integer> prefix) {
        switch (node.getType()) {
            case hold:
                Hold internalNode = (Hold) node;

                prefix.add(0);
                buildCodeList(internalNode.getLeft(), prefix);
                prefix.remove(prefix.size() - 1);

                prefix.add(1);
                buildCodeList(internalNode.getRight(), prefix);
                prefix.remove(prefix.size() - 1);

                break;
            case leaf:
                Leaf leaf = (Leaf) node;

                if (leaf.getChunk() >= codes.size()) throw new IllegalArgumentException("Чанк превысил лимит размера");
                if (codes.get(leaf.getChunk()) != null) throw new IllegalArgumentException("Чанк имеет более одного кода");

                codes.set(leaf.getChunk(), new ArrayList<>(prefix));

                break;
            default:
                System.out.println("Неизвестный тип ноды. Проехали.");
        }
    }

    public List<Integer> getCode(int symbol) {
        if (symbol < 0) throw new IllegalArgumentException("Чанк не может быть отрицательным");

        else if (codes.get(symbol) == null) throw new IllegalArgumentException("Отсутствует код для данного чанка.");

        else return codes.get(symbol);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        toString("", root, sb);
        return sb.toString();
    }

    private static void toString(String prefix, Node node, StringBuilder sb) {
        switch (node.getType()) {
            case hold:
                Hold holder = (Hold) node;
                toString(prefix + "0", holder.getLeft(), sb);
                toString(prefix + "1", holder.getRight(), sb);

                break;
            case leaf:
                sb.append(String.format("Код %s: Чанк %d%n", prefix, ((Leaf) node).getChunk()));

                break;
            default:
                System.out.println("Неизвестный тип ноды. Проехали.");
        }
    }

    public Node getRoot() {
        return root;
    }

    public List<List<Integer>> getCodes() {
        return codes;
    }

}
